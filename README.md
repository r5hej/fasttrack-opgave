# Fast track opgave
Et lille program som gør brug af  [SWAPI](https://swapi.dev). Jeg har kun gjort brug af People API'et. Programmet har et CLI interface som bruges til query efter en karakter fra People API'et.

Programmet er skrevet i python3.8.

**Et par eksempler:**

Kørsel uden argumenter
```shell
python main.py 

Usage: main.py [OPTIONS] COMMAND [ARGS]...

Options:
  -c, --use-cache  Use the local cache
  --help           Show this message and exit.

Commands:
  people
```

Query karakter med navn "luke skywalker"
```shell
python main.py people name "luke skywalker"
```

Query karakter med navn "luke skywalker" med lokal cache
```shell
python main.py -c people name "luke skywalker"
```

Query karakter med id 23
```shell
python main.py people id 23
```
