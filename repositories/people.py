from typing import List, Optional, Dict
from .swapi import SwapiRepository
import json
import requests
import logger
import re
import pickle

class PeopleRepository(SwapiRepository):
    _people_cache: Dict[int, dict] = {}
    _cache_file_name = '.people_cache'

    def __init__(self) -> None:
        super().__init__()
        self._people_url = f'{super().base_url}/people/'

    def load_local_cache(self) -> None:
        try:
            with open(f'./{self._cache_file_name}', 'rb') as cache_f:
                self._people_cache = pickle.load(cache_f)
        except:
            logger.log(logger.LOG_LEVEL.INFO, 'Could not find cache file. Creating from remote')
            self.reload_cache_from_remote()

    def save_to_local_cache(self) -> None:
        logger.log(logger.LOG_LEVEL.INFO, 'Saving cache to local file: ' + self._cache_file_name)
        with open(f'./{self._cache_file_name}', 'wb') as cache_f:
            pickle.dump(self._people_cache, cache_f)

    
    def reload_cache_from_remote(self) -> None:
        """
        Preload all star wars people to local cache
        This will override existing local cache
        
        - return: true if preload was successfull else false
        """
        url: Optional[str] = f'{self._people_url}?page=1'
        while(url is not None):
            logger.log(logger.LOG_LEVEL.INFO, f'Preloading Person(s) from remote: {url}')
            response = requests.get(url)
            if response.status_code != 200:
                logger.log(logger.LOG_LEVEL.ERROR, f'Preload failed. Reponse status code: {response.status_code}')
                logger.log(logger.LOG_LEVEL.ERROR, f'Error message: {response.text}')
                url = None
            
            body = json.loads(response.text)
            url = None if body['next'] is None else body['next']

            for person in body['results']:
                self._add_to_cache(person)

            logger.log(logger.LOG_LEVEL.INFO, f'Parsed Person batch. New cache size: {len(self._people_cache)}')
        self.save_to_local_cache()
        
        
    def _add_to_cache(self, person: dict) -> None:
        id_match = re.match('.*/(?P<id>\d+)/', person['url'])
        if id_match is None:
            raise Exception('Person id parsing for local cache error. Regex did not find a people id')
        
        person_id = int(id_match.group('id'))
        self._people_cache[person_id] = person

    def fetch_person_by_id(self, id: int) -> Optional[dict]:
        if id is None:
            return None
        
        if id in self._people_cache:
            logger.log(logger.LOG_LEVEL.INFO, f'Using local cache for Person id: {id}')
            return self._people_cache.get(id)


        logger.log(logger.LOG_LEVEL.INFO, f'No local cache for Person id: {id}. Fetching from remote')
        url = f'{self._people_url}{id}'
        response = requests.get(url)
        if response.status_code == 200:
            response_body = json.loads(response.text)
            self._add_to_cache(response_body)
            return response_body
        
        return None
    
    def fetch_person_by_name(self, name: str) -> List[dict]:
        if name is None or name == '':
            return []
        
        
        for person in self._people_cache.values():
            if name.casefold() in person['name'].casefold():
                logger.log(logger.LOG_LEVEL.INFO, f'Using local cache for Person name: {name}')
                return person

        logger.log(logger.LOG_LEVEL.INFO, f'No local cache for Person name: {name}. Fetching from remote')
        url = f'{self._people_url}?search={name}'
        response = requests.get(url)
        if response.status_code != 200:
            return []

        response_body = json.loads(response.text)
        if len(response_body['results']) == 0:
            return []
        
        for person in response_body['results']:
            self._add_to_cache(person)
        return response_body['results']
        
        