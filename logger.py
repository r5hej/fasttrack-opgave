from enum import Enum
from datetime import datetime

LOG_LEVEL = Enum('Log level', ['INFO', 'ERROR'])

def log(level: LOG_LEVEL, message: str) -> None:
    print(f'[{level.name}][{datetime.now()}] {message}')
    

        