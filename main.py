from repositories.people import PeopleRepository
import logger
import click

people_repo = PeopleRepository()

@click.group()
@click.option('-c', '--use-cache', default=False, show_default=True, is_flag=True, help='Use the local cache')
def cli(use_cache):
    if use_cache:
        logger.log(logger.LOG_LEVEL.INFO, 'Loading local people cache')
        people_repo.load_local_cache()

@cli.group()
def people():
    pass

@people.command()
@click.argument('name')
def name(name: str) -> None:
    result = people_repo.fetch_person_by_name(name)
    print(f'Found star wars people:\n{result}')


@people.command()
@click.argument('id')
def id(id) -> None:
    result = people_repo.fetch_person_by_id(int(id))
    print(f'Found star wars people:\n{result}')


if __name__ == '__main__':
    cli()